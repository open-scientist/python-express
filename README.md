# Python express

Cette formation est à destination des personnes n'ayant que peu d'expérience
en programmation et analyse de données.

À l'issue de cette formation, vous aurez les éléments de base pour débuter
l'analyse de données, en particulier issues de jeux de données en Open Data.

Le langage des exemples est Python, pour la facilité à débuter dans ce langage.
En revanche, nombre des bonnes habitudes présentées dans cette formation sont
génériques et s'appliquent à tout langage.


## Tutoriel

Une première version du tutoriel est en cours de création sur la branche 
[urfist-strasbourg/cocreation](https://gitlab.com/open-scientist/python-express/tree/urfist-strasbourg/cocreation)